% This file should be included by one of the specific SilTeX classes.

% Basic packages
\RequirePackage{l3keys2e, xparse, xkeyval}
\ProvidesExplClass
	{siltex-common}
	{2018/11/11}
	{1.0}
	{Rolling Release SilTeX document class}

% SilTeX macro
\cs_new_protected:Npn \siltexlogo {
	\addfontfeature{LetterSpace=0} Sil\kern-.1ex\TeX
}

\tl_set:Nn \SilTeX {
	\texorpdfstring { \siltexlogo } { SilTeX }
}

% Switch for twoside
\bool_new:N \siltex_twoside

% Class options
\keys_define:nn { siltex_options } {
	package     .bool_set:N = \siltex_package,
	nopackage   .bool_set_inverse:N = \siltex_package,
	titlepage   .bool_set:N = \siltex_titlepage,
	titlecaps   .bool_set:N = \siltex_titlecaps,
	raggedright .bool_set:N = \siltex_raggedright,
	fullwidth   .bool_set:N = \siltex_fullwidth,
	nomath      .code:n =
		\PassOptionsToPackage{nomath}{siltex},
	twoside     .code:n =
		\bool_set_true:N \siltex_twoside
		\PassOptionsToClass{twoside}{\siltex_class},
	unknown     .code:n =
		\PassOptionsToClass{\CurrentOption}{\siltex_class},
}

% Set default options
\keys_set:nn{ siltex_options }{ package }

% Process options
\ProcessKeysOptions{ siltex_options }

% Load requested class
\LoadClass[a4paper]{\siltex_class}

% Packages
% Commented packages may be required elsewhere in this class
\RequirePackage{
	geometry,	% Page layout'
	setspace,	% Change line spacing
	fancyhdr,	% Header/footer
	textcase,	% Change the case of text properly
	titling,	% Retrieve author and title
	placeins,	% For \FloatBarrier to keep numbers in sequence
	caption,	% For customising captions
	sectsty,	% Header styling
	marginnote,	% Proper notes in the margin
	marginfix,  % Fix issues with \marginpar
%	mcaption,	% Caption in margin for regular figures
%	changepage,	% Change page layout halfway -- loaded by mcaption
%	tocloft,	% Customise table of contents
	ragged2e,	% Proper text justification
}

%% Geometry
% Page geometry
\bool_new:N \siltex_fullwidthgeometry
\DeclareDocumentCommand \fullwidthgeometry {} {
	\bool_set_true:N \siltex_fullwidthgeometry
	\newgeometry{
		heightrounded,
		vmargin    = 25mm,
		hmargin    = 22mm,
		headsep    = 20pt,
		headheight = 15pt
	}
	\fancyhfoffset[L,R]{0mm}
}
\DeclareDocumentCommand \nofullwidthgeometry {} {
	\bool_set_false:N \siltex_fullwidthgeometry
}

\bool_if:NTF \siltex_fullwidth {
	\bool_set_true:N \siltex_fullwidthgeometry
	\geometry{
		heightrounded,
		vmargin    = 25mm,
		hmargin    = 22mm,
		headsep    = 20pt,
		headheight = 15pt
	}
} {
	\geometry{
		heightrounded,
		vmargin        = 25mm,
		outer          = 80mm,
		inner          = 22mm,
		marginparwidth = 50mm,
		marginparsep   = 8mm,
		headsep        = 20pt,
		headheight     = 15pt
	}
}

% Header/footer
\tl_new:N \siltex_header_left
\tl_new:N \siltex_header_right
\tl_set:Nn \headrulewidth {0pt}
\tl_set:Nn \footrulewidth {0pt}
\pagestyle{fancy}
\fancyhf{}

% Set the right information for the header
\str_case:on {\siltex_class} {
	{ article } {
		\tl_set:Nn \siltex_header_left  {\newlinetospace{\theauthor}}
		\tl_set:Nn \siltex_header_right {\newlinetospace{\thetitle}}
	}
	{ report } {
		\tl_set:Nn \siltex_header_left  {\newlinetospace{\thetitle}}
		\tl_set:Nn \siltex_header_right {\nouppercase{\leftmark}}
	}
        { book } {
		\tl_set:Nn \siltex_header_left  {\newlinetospace{\thetitle}}
		\tl_set:Nn \siltex_header_right {\nouppercase{\leftmark}}
	}
}

\bool_if:NTF \siltex_twoside {
	\fancyhfoffset[LE,RO]{58mm}
	\fancyhead[LE]{ \headerfont{\thepage \quad \siltex_header_left} }
	\fancyhead[RO]{ \headerfont{\siltex_header_right \quad \thepage} }
} {
	\fancyhfoffset[R]{58mm}
	\fancyhead[L]{ \headerfont{\siltex_header_left} }
	\fancyhead[R]{ \headerfont{\siltex_header_right \quad \thepage} }
}

\bool_if:NT \siltex_fullwidth
	{ \fancyhfoffset[L,R]{0mm} }

% On the first page of a chapter, only the page number should be shown
\fancypagestyle {plain} {
	\fancyhf{}
	\bool_if:NTF \siltex_twoside {
		\fancyhfoffset[LE,RO]{58mm}
		\fancyhead[LE,RO]{ \headerfont{\thepage} }
	} {
		\fancyhfoffset[R]{58mm}
		\fancyhead[R]{ \headerfont{\thepage} }
	}
}

% Table of contents
\RequirePackage[titles]{tocloft}
\tl_set:Nn \cftpartfont          {\tocfont}
\tl_set:Nn \cftpartpagefont      {\tocfont}
\tl_set:Nn \cftsecfont           {\tocfont}
\tl_set:Nn \cftsecpagefont       {\tocfont}
\tl_set:Nn \cftsubsecfont        {\tocfont}
\tl_set:Nn \cftsubsecpagefont    {\tocfont}
\tl_set:Nn \cftsubsubsecfont     {\tocfont}
\tl_set:Nn \cftsubsubsecpagefont {\tocfont}

\str_if_eq:VnT \siltex_class {report} {
	\tl_set:Nn \cftchapfont     {\bfseries\tocfont}
	\tl_set:Nn \cftchappagefont {\bfseries\tocfont}
}
\str_if_eq:VnT \siltex_class {book} {
	\tl_set:Nn \cftchapfont     {\bfseries\tocfont}
	\tl_set:Nn \cftchappagefont {\bfseries\tocfont}
}

%% New environments
\RequirePackage[top]{mcaption}

\DeclareCaptionFont {marginfont}  { \marginfont }
\DeclareCaptionTextFormat {hfill} { #1 \hfill }
\captionsetup{
	font            = marginfont,
	textformat      = hfill,
	justification   = RaggedRight,
	singlelinecheck = off
}

\cs_set_eq:NN \siltex_marginnote_save \marginnote

\DeclareDocumentCommand \margincaption { O{} m O{-\baselineskip} }
	{ \siltex_marginnote_save{ \caption[#1]{#2} }[#3] }

\DeclareDocumentCommand \marginnote { m O{0ex} }
	{ \marginpar{ \vspace{#2} \marginfont #1 } }

% Modified footnote for in the margin
\DeclareDocumentCommand \sidenote { o m O{0ex} }{
	\IfNoValueTF {#1} {
		\stepcounter \@mpfn
		\protected@xdef \@thefnmark{\thempfn}
	}{
		\begingroup
			\csname c@\@mpfn\endcsname #1\relax
			\unrestored@protected@xdef\@thefnmark{\thempfn}
		\endgroup
	}
	\@footnotemark
    \marginnote{
		\normalfont\marginfont
		\noindent{\@makefnmark} ~ \normalfont \marginfont #2
    }[#3]
}

% Originals and variants
\bool_if:NF \siltex_fullwidth {
	\RenewDocumentEnvironment {figure} { O{htbp} }
		{ \begin{regularfloat}{figure}[#1] }
		{ \end{regularfloat} }

	\RenewDocumentEnvironment {table} { O{htbp} }
		{ \begin{regularfloat}{table}[#1] }
		{ \end{regularfloat} }
}

\RenewDocumentEnvironment {figure*} { O{htbp} }
	{ \begin{fullwidthfloat}{figure}[#1] \captionsetup{singlelinecheck=off} }
	{ \end{fullwidthfloat} }

\RenewDocumentEnvironment {table*} { O{htbp} }
	{ \begin{fullwidthfloat}{table}[#1] }
	{ \end{fullwidthfloat} }

% Regular float
\DeclareDocumentEnvironment {regularfloat} { m O{htbp} }{
	\@float{#1}[#2]
	\bool_if:NF \siltex_fullwidthgeometry
		{ \margincap }
}{
	\bool_if:NF \siltex_fullwidthgeometry
		{ \endmargincap }
	\end@float
}

% Fullwidth
\DeclareDocumentEnvironment {fullwidth} {} {
	\bool_if:NTF \siltex_twoside
		{ \begin{adjustwidth*}{}{-58mm} }
		{ \begin{adjustwidth}{}{-58mm} }

}{
	\bool_if:NTF \siltex_twoside
		{ \end{adjustwidth*} }
		{ \end{adjustwidth} }
}

\DeclareDocumentEnvironment {fullwidthfloat} { m O{htbp} } {
	\bool_if:NTF \siltex_fullwidthgeometry
		{ \@dblfloat{#1}[#2] }
		{ \@float{#1}[#2] \fullwidth }
}{
	\bool_if:NTF \siltex_fullwidthgeometry
		{ \end@dblfloat }
		{ \endfullwidth \end@float }
}
\DeclareDocumentEnvironment {fullwidthpage} {}
	{ \fullwidthgeometry }
	{ \restoregeometry }

% Margin
\newsavebox \siltex_marginfloat_box
\DeclareDocumentEnvironment {marginfloat} { m O{0mm} }{
	\FloatBarrier
	\begin{lrbox}{\siltex_marginfloat_box}
	\begin{minipage}{\marginparwidth}
	\marginfont
	\captionsetup{type=#1}
	\vspace*{#2}
	\noindent
}{
	\end{minipage}
	\end{lrbox}
	\marginpar{\noindent\usebox{\siltex_marginfloat_box}}
}

\DeclareDocumentEnvironment {marginfigure} { O{0ex} }
	{ \begin{marginfloat}{figure}[#1] }
	{ \end{marginfloat} }

\DeclareDocumentEnvironment {margintable} { O{0ex} }
	{ \begin{marginfloat}{table}[#1] }
	{ \end{marginfloat} }

%% Miscellaneous commands
% Replace all newlines by spaces (experimental)
\DeclareDocumentCommand \newlinetospace { m } {
	% Store expanded argument in token list
	\tl_set:Nf \l_tmpa_tl {#1}

	% Replace newlines and protects (which can separate spaces)
	\regex_replace_all:nnN {(\c{newline|protect}|\c{\\})} {\ } \l_tmpa_tl

	% Filter whitespace
	\regex_replace_all:nnN {\s+} {\ } \l_tmpa_tl

	% Use result
	\tl_use:N \l_tmpa_tl
}

%% Font styling, etc...
% Font hooks
\allsectionsfont{\sectionfont}
\tl_new:N  \headerfont
\tl_new:N  \titlefont
\tl_new:N  \tocfont
\tl_set:Nn \sectionfont {}
\tl_set:Nn \marginfont {\footnotesize}
\DeclareDocumentCommand \setsectionfont { m } { \tl_set:Nn \sectionfont {#1} }
\DeclareDocumentCommand \setheaderfont  { m } { \tl_set:Nn \headerfont  {#1} }
\DeclareDocumentCommand \setmarginfont  { m } { \tl_set:Nn \marginfont  {#1} }
\DeclareDocumentCommand \settitlefont   { m } { \tl_set:Nn \titlefont   {#1} }
\DeclareDocumentCommand \settocfont     { m } { \tl_set:Nn \tocfont     {#1} }

% Font justification and indentation
\bool_if:NT \siltex_raggedright {
	\PassOptionsToPackage{newcommands,raggedrightboxes,footnotes,document}{ragged2e}
}
\RequirePackage{ragged2e}

% New Commands with proper spacing
\DeclareDocumentCommand \textallcaps { m }
	{ {\addfontfeature{LetterSpace=15}\MakeTextUppercase{#1}} }

\DeclareDocumentCommand \textsmallcaps { m }
	{ {\addfontfeature{Letters=SmallCaps,LetterSpace=10}\MakeTextLowercase{#1}} }

\DeclareDocumentCommand \fakesc{ m } {{
	\xdef\fake_name{\csname\curr@fontshape/\f@size\endcsname}
	\fontsize{\fontdimen8\fake_name}{\baselineskip}\selectfont
	\addfontfeature{LetterSpace=10}\MakeUppercase{#1}
}}

%% Maketitle commands
\tl_new:N \thesubtitle
\tl_new:N \thepublisher
\DeclareDocumentCommand \subtitle  { m } { \tl_set:Nn \thesubtitle  {#1} }
\DeclareDocumentCommand \publisher { m } { \tl_set:Nn \thepublisher {#1} }

% Maketitle
\bool_if:NTF \siltex_titlepage {
	\RenewDocumentCommand \maketitle {} {
		\begin{titlepage}
			% Only use \newline here
			\cs_set_eq:NN \\ \newline
			\dim_set:Nn \parindent {0pt}

			% Geometry for the title page
			\newgeometry{
				left = 22mm,
				textwidth = 115mm,
				vmargin = 25mm,
				heightrounded,
			}

			% Change font/spacing
			\titlefont
			\Huge
			\onehalfspacing

			% Font size for the author
			{\LARGE \theauthor}
			\par \vspace{50mm}

			% Title
			\bool_if:NTF \siltex_titlecaps {
				\textallcaps{\thetitle}
				\par \vspace{2mm}
				{ \huge \textallcaps{\thesubtitle} }
			} {
				\thetitle
				\par \vspace{1mm}
				{ \huge \thesubtitle }
			}

			\vfill
			{ \large \par \thepublisher }
		\end{titlepage}
		\restoregeometry
	}
} {
	\cs_set_eq:NN \siltex_maketitle_save \maketitle
	\RenewDocumentCommand \maketitle {} {
		{ \titlefont \siltex_maketitle_save }
	}
}

% Load SilTeX package
\bool_if:NT \siltex_package
	{ \RequirePackage{siltex} }
