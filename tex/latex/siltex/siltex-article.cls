\RequirePackage{ expl3 }
\ProvidesExplClass
	{siltex-article}
	{2017/08/23}
	{1.0}
	{Rolling Release SilTeX document class}

\tl_set:Nn\siltex_class{article}
\tl_set:Nn\siltex_options{}

\LoadClassWithOptions{siltex-common}
