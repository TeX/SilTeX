\RequirePackage{ expl3 }
\ProvidesExplClass
	{siltex-book}
	{2018/11/11}
	{1.0}
	{Rolling Release SilTeX document class}

\tl_set:Nn\siltex_class{book}
\tl_set:Nn\siltex_options{}

\LoadClassWithOptions{siltex-common}
