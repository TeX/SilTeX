% !TEX spellcheck = en_GB
\documentclass[12pt,titlecaps,nopackage,english]{siltex-article}
\usepackage[nofonts]{siltex}
\usepackage{tabularx,babel}
\usepackage[backend=biber,style=ieee]{biblatex}
%\usepackage[style,image=8,imageheight=1.9\paperheight,imageoffset=-.5\paperheight]{utwentetitle}
\usepackage{utwentetitle}

% Redefine Emphasis
\setem{\bfseries}[bx][\normalfont\scshape]

% Bibliography
\bibliography{bibliography}

% Author etc
\author{Silex}
\title{\SilTeX}
%\subtitle{•Document class \\ •Package}
\subtitle{empty}
%\publisher{SLXH.nl}

\listfiles
\begin{document}
\maketitle

\section*{Introduction}
\Lettrine{The} \SilTeX\ document and package are my personal templates for documents. This means they are constantly changing. They provide a nice, consistent base for many, if not all, documents and load the most important packages for you. \SilTeX\ is split in a document class, which does most of the layout, and a package, which does most of the styling. Both can be used independently. To show you how easy it is to create a nice looking document with \SilTeX, here is most of the preamble of this document:

\lstinputlisting[language={[LaTeX]TeX},linerange=2-3]{siltex_new.tex}

\tableofcontents

\clearpage
\section{Document Class}
\Lettrine{The} \SilTeX\ document class is meant to be a very simple patch over either {the \emph{article} or \emph{report} class. It loads only the necessary packages and tries to keep modifications as simple as possible.

\subsection{Options}
The document class provides the following options. All other options are passed to the super-class (\texttt{article} or \texttt{report}, depending on the \SilTeX\ class).
\begin{table}[ht!]
\caption{Options for the document class. \newline *: default}
\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\linewidth}{>{\bfseries}p{5em} X}
	Option & Description \\ \hline
	package* & Load the \SilTeX\ package.\\
	nopackage & Don’t load the \SilTeX\ package.\\
	titlecaps & Use all caps in the title and subtitle.\\
	raggedright & Set all text as ragged right (left aligned).\\
	fullwidth & Page layout without margins.\\
	twoside & Use two sided layout. Also passed to the super-class.\\
	titlepage & Use a title page.
\end{tabularx}
\end{table}

\subsection{Packages}
The document class loads the following packages:

\fullwidthlstinputlisting[language={[LaTeX]TeX},linerange={38-58}]{siltex-common.cls}

For more information about these packages, look at the documentation of that package.

\subsection{Commands}
The document class provides the following commands:
\begin{table}[ht]
\caption{Commands provided by the document class}
\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\linewidth}{p{10em} X}
	Command & Description \\ \hline
	\cmd{SilTeX} & \SilTeX\ logo.\\
	\cmd{fullwidthgeometry} & Sets the geometry to full-width.\\
	\cmd{margincaption} & A caption in the margin.\\
	\cmd{sectionfont}, \newline
	\cmd{headerfont}, \newline
	\cmd{marginfont}, \newline
	\cmd{titlefont}, \newline
	\cmd{tocfont} & Font hooks for setting the font commands for sections, headers, margins, titles and the table of contents respectively.\\
	\cmd{setsectionfont}, \newline
	\cmd{setheaderfont}, \newline
	\cmd{setmarginfont}, \newline
	\cmd{settitlefont}, \newline
	\cmd{settocfont} & Easy commands to set all font hooks.\\
	\cmd{textallcaps} & Transform text to uppercase with proper spacing.\\
	\cmd{textsmallcaps} & Transform text to lowercase small caps with spacing.\\
	\cmd{faksesc} & Fake small capitals.\\
	\cmd{newlinetospace} & Filter newlines from text and transform them to spaces.\\
	\cmd{subtitle} & The custom title page has room for a subtitle.\\
	\cmd{publisher} & The custom title page also has room for a publisher.
\end{tabularx}
\end{table}

It also modifies the following commands:

\begin{table}[ht]
\caption{Commands modified by the document class}
\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\linewidth}{p{10em} X}
	Command & Description \\ \hline
	\cmd{maketitle} & With the \emph{titlepage} option, the command is modified to provide the title page seen in this document. \\
\end{tabularx}
\end{table}

\subsection{Environments}

The document class provides the following environments:

\begin{table}[ht]
\caption{Environments provided by the document class}
\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\linewidth}{p{6em} X}
	Environment & Description \\ \hline
	figure* & A full with version of the \emph{figure} environment. \\
	table* & A full with version of the \emph{table} environment. \\
	regularfloat & A floating environment within the margin. \\
	fullwidth & A full width environment. \\
	fullwidthfloat & A full width floating environment. \\
	fullwidthpage & Full width spanning pages. \\
	marginfloat & A float in the margin. \\
	marginfigure & A figure in the margin. \\
	margintable & A table in the margin. \\
\end{tabularx}
\end{table}

It also modifies the following environments:

\begin{table}[ht]
\caption{Environments modified by the document class}
\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\linewidth}{p{6em} X}
	Environment & Description \\ \hline
	figure & The caption is placed in the margin. \\
	table & The caption is placed in the margin. \\
\end{tabularx}
\end{table}


\clearpage
\section{Package}
\Lettrine{The} \SilTeX\ package uses all hooks provided by the \SilTeX\ document class to provide a nice looking format. It can also be loaded on top of regular document classes, but in that case it won’t apply some changes.

\subsection{Options}
The document class provides the following options. All other options are passed to the superclass (\texttt{article} or \texttt{report}, depending on the \SilTeX\ class).

\begin{table}[ht!]
\caption{Options for the package. \newline *: default}
\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\linewidth}{>{\bfseries}p{6em} X}
	Environment & Description \\ \hline
	cite* & Replace \cmd{cite} with \cmd{wikicite}.\\
	nocite & Don’t replace \cmd{cite}.\\
	fonts* & Load Vollkorn, Source Sans Pro and Source Code Pro. Default.\\
	nofonts & Don’t load fonts.\\
	serif & Load Vollkorn as default font.\\
	noserif* & Load Source Sans Pro as default font.\\
	math* & Replace default math fonts with the document fonts. Requires the \textbf{fonts} option.\\
	nomath & Don’t replace math fonts.\\
	sfmath & Make all math sans-serif. \\
\end{tabularx}
\end{table}

\subsection{Packages}
The document class loads the following packages:
\fullwidthlstinputlisting[language={[LaTeX]TeX},linerange=30-56]{siltex.sty}


For more information about these packages, look at the documentation of that package.

\subsection{Commands}
The package provides the following commands:
\begin{table}[ht]
\caption{Commands provided by the package}
\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\linewidth}{p{10em} X}
	Command & Description \\ \hline
	\cmd{SilTeX} & \SilTeX\ logo \\
	\cmd{marginlst\-inputlisting} & \cmd{lstinputlisting} for in the margin. \\
	\cmd{Lettrine}, \newline
	\cmd{dropcap}, \newline
	\cmd{initial} & A version of \cmd{lettrine} that requires only one argument. \\
	\cmd{textoverline} & An overline in regular text. \\
	\cmd{wikicite}, \newline
	\cmd{wcite} & Citations in the Wikepedia style.\\
	\cmd{fullwidthlst\-inputlisting} & \cmd{lstinputlisting} in full width.\\
	\cmd{setem} & Set the emphasis. Example:
		\texttt{\cmd{setem}\{\cmd{bfseries}\}[bx]
			[\cmd{normalfont}\cmd{scshape}]} \\
	\cmd{emdefault} & Default shape for emphasis. \\
\end{tabularx}
\end{table}

\clearpage

It modifies the following commands:
\begin{table}[ht]
\caption{Commands modified by the package}
\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\linewidth}{p{6em} X}
	Command & Description \\ \hline
	\cmd{em} &  \\
	\cmd{cite} & With the \emph{cite} option, the command is replaced by \cmd{wikicite}. \\
\end{tabularx}
\end{table}

\subsection{Environments}

The package provides the following environments:

\begin{table}[ht!]
\caption{Environments provided by the package}
\renewcommand{\arraystretch}{1.5}%
\begin{tabularx}{\linewidth}{p{6em} X}
	Environment & Description \\ \hline
	marginlisting &  \\
\end{tabularx}
\end{table}

\clearpage
\section{Current issues}
\begin{itemize}
	\item \cmd{fullwidthlstinputlisting} breaks (in) draft mode. This has been overridden by disabling draft mode for this command.
\end{itemize}

\appendices
\section{Debugging}

\setem{\itshape}[it][\normalfont]
So, this is another test in texpad.

This is a test \emph{for emphasis \emph{in emphasis \emph{in emphasis}, it kind of} feels like} inception.

\setem{\bfseries}[bx][\normalfont\scshape]

This is a test \emph{for emphasis \emph{in emphasis \emph{in emphasis}, it kind of} feels like} inception.

This is a citation\cite{dorgan99}, and here is a citation necessary\cite{needed}. It works quite ok\cite{keller08}.

\printbibliography[heading=bibintoc,heading=bibnumbered]

\end{document}