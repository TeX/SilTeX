% !TEX spellcheck = en_GB
\documentclass[12pt,english,titlepage,titlecaps]{siltex-article}
\usepackage{babel,tabularx,lipsum}

% Redefine Emphasis
\setem{\bfseries}[bx][\normalfont\scshape]

% Simple macro
\DeclareDocumentCommand\cmd{m}{\texttt{\textbackslash#1}}

% Use more whitespace in tables
\renewcommand{\arraystretch}{1.5}

% Column for options/environments
\newcolumntype{O}{>{\bfseries}p{5em}}

% Author etc
\author{Silex}
\title{\SilTeX}
\subtitle{%
	•Document class \\
	•Package
}
\publisher{SLXH.nl}

\begin{document}
\maketitle

\section*{Introduction}
\Lettrine{The} \SilTeX\ document and package are my personal templates for documents.
This means they are constantly changing.
They provide a nice, consistent base for many, if not all, documents and load the most important packages for you.
\SilTeX\ is split in a document class, which does most of the layout, and a package, which does most of the styling.
Both can be used independently.
To show you how easy it is to create a nice looking document with \SilTeX, here is most of the preamble of this document:

\lstinputlisting[language={[LaTeX]TeX},linerange=2-3]{siltex.tex}

\tableofcontents

\clearpage
\section{Document Class}
\Lettrine{The} \SilTeX\ document class is meant to be a very simple patch over either {the \emph{article} or \emph{report} class.
It loads only the necessary packages and tries to keep modifications as simple as possible.

\subsection{Options}
The document class provides the following options.
All other options are passed to the super-class (\texttt{article} or \texttt{report}, depending on the \SilTeX\ class).

\begin{tabularx}{\linewidth}{O X}
	\toprule
	Option      & Description \\
	\midrule
	package     & Load the \SilTeX\ package (default). \\
	nopackage   & Don’t load the \SilTeX\ package. \\
	titlecaps   & Use all caps in the title and subtitle. \\
	raggedright & Set all text as ragged right (left aligned). \\
	fullwidth   & Page layout without margins. \\
	twoside     & Use two sided layout. Also passed to the super-class. \\
	titlepage   & Use a title page. \\
	\bottomrule
\end{tabularx}

\subsection{Packages}
The document class loads the following packages:

\fullwidthlstinputlisting[language={[LaTeX]TeX},linerange={43-57}]{siltex-common.cls}

For more information about these packages, look at the documentation of that package.

\subsection{Commands}
The document class provides the following commands:

\begin{tabularx}{\linewidth}{p{10em} X}
	\toprule
	Command                 & Description \\
	\midrule
	\cmd{SilTeX}            & \SilTeX\ logo. \\
	\cmd{fullwidthgeometry} & Sets the geometry to full-width. \\
	\cmd{margincaption}     & A caption in the margin. \\
	\cmd{sectionfont},      \newline
	\cmd{headerfont},       \newline
	\cmd{marginfont},       \newline
	\cmd{titlefont},        \newline
	\cmd{tocfont}           & Font hooks for setting the font commands for sections, headers,
	                          margins, titles and the table of contents respectively.\\
	\cmd{setsectionfont},   \newline
	\cmd{setheaderfont},    \newline
	\cmd{setmarginfont},    \newline
	\cmd{settitlefont},     \newline
	\cmd{settocfont}        & Easy commands to set all font hooks.\\
	\cmd{textallcaps}       & Transform text to uppercase with proper spacing.\\
	\cmd{textsmallcaps}     & Transform text to lowercase small caps with spacing.\\
	\cmd{faksesc}           & Fake small capitals.\\
	\cmd{newlinetospace}    & Filter newlines from text and transform them to spaces.\\
	\cmd{subtitle}          & The custom title page has room for a subtitle.\\
	\cmd{publisher}         & The custom title page also has room for a publisher. \\
	\bottomrule
\end{tabularx}

It also modifies the following commands:

\begin{tabularx}{\linewidth}{p{10em} X}
	\toprule
	Command         & Description \\
	\midrule
	\cmd{maketitle} & With the \emph{titlepage} option,
	                  the command is modified to provide the title page seen in this document. \\
	\bottomrule
\end{tabularx}

\subsection{Environments}

The document class provides the following environments:

\begin{tabularx}{\linewidth}{p{6em} X}
	\toprule
	Environment & Description \\
	\midrule
	figure         & A full with version of the \emph{figure} environment (default). \\
	table          & A full with version of the \emph{table} environment (default). \\
	regularfloat   & A floating environment within the margin. \\
	fullwidth      & A full width environment. \\
	fullwidthfloat & A full width floating environment. \\
	fullwidthpage  & Full width spanning pages. \\
	marginfloat    & A float in the margin. \\
	marginfigure   & A figure in the margin. \\
	margintable    & A table in the margin. \\
	\bottomrule
\end{tabularx}

It also modifies the following environments:

\begin{tabularx}{\linewidth}{p{6em} X}
	\toprule
	Environment & Description \\
	\midrule
	figure      & The caption is placed in the margin. \\
	table       & The caption is placed in the margin. \\
	\bottomrule
\end{tabularx}

\clearpage

\section{Package}
\Lettrine{The} \SilTeX\ package uses all hooks provided by the \SilTeX\ document class to provide a nice looking format.
It can also be loaded on top of regular document classes,
but in that case it won’t apply some changes.

\subsection{Options}
The document class provides the following options.
All other options are passed to the superclass
(\texttt{article} or \texttt{report}, depending on the \SilTeX\ class).

\begin{tabularx}{\linewidth}{O X}
	\toprule
	Environment & Description \\
	\midrule
	cite        & Replace \cmd{cite} with \cmd{wikicite} (default).\\
	nocite      & Don’t replace \cmd{cite}.\\
	fonts       & Load Vollkorn, Source Sans Pro and Source Code Pro (default). \\
	nofonts     & Don’t load fonts. \\
	serif       & Load Vollkorn as default font. \\
	noserif     & Load Source Sans Pro as default font (default). \\
	math        & Replace default math fonts with the document fonts.
	              Requires the \textbf{fonts} option (default). \\
	nomath      & Don’t replace math fonts. \\
	sfmath      & Make all math sans-serif. \\
	\bottomrule
\end{tabularx}

\subsection{Packages}
The document class loads the following packages:
\fullwidthlstinputlisting[language={[LaTeX]TeX},linerange=35-60]{siltex.sty}

For more information about these packages, look at the documentation of that package.

\subsection{Commands}
The package provides the following commands:

\begin{tabularx}{\linewidth}{p{10em} X}
	\toprule
	Command                        & Description \\
	\midrule
	\cmd{SilTeX}                   & \SilTeX\ logo \\
	\cmd{marginlst\-inputlisting}  & \cmd{lstinputlisting} for in the margin. \\
	\cmd{Lettrine},                \newline
	\cmd{dropcap},                 \newline
	\cmd{initial}                  & A version of \cmd{lettrine} that requires only one argument. \\
	\cmd{textoverline}             & An overline in regular text. \\
	\cmd{wikicite},                \newline
	\cmd{wcite}                    & Citations in the Wikepedia style.\\
	\cmd{fullwidthlst\-inputlisting} & \cmd{lstinputlisting} in full width.\\
	\cmd{setem}                    & Set the way emphasis works. \\
	\cmd{emdefault}                & Default shape for emphasis. \\
	\bottomrule
\end{tabularx}

\clearpage

It modifies the following commands:

\begin{tabularx}{\linewidth}{p{6em} X}
	\toprule
	Command    & Description \\
	\midrule
	\cmd{em}   & Emphasis font shape. \\
	\cmd{cite} & With the \emph{cite} option, the command is replaced by \cmd{wikicite}. \\
	\bottomrule
\end{tabularx}

\subsection{Environments}

The package provides the following environments:

\begin{tabularx}{\linewidth}{p{6em} X}
	\toprule
	Environment   & Description \\
	\midrule
	marginlisting &  \\
	\bottomrule
\end{tabularx}

\clearpage

\section{Current issues}

\begin{itemize}
	\item \cmd{fullwidthlstinputlisting} breaks (in) draft mode.
	      This has been overridden by disabling draft mode for this command.
\end{itemize}

\appendices

\end{document}
